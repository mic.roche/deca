// import axios from 'axios'

// const API_URL = process.env.API_URL || ''
// const apiClient = axios.create({
//     baseURL: API_URL,
//     withCredentials: false,
// })


export default {
    async login(credentials) {
        let retour = {
            superadmin: false,
            user: {
                nom:null,
                prenom:null,
                email:null
            },
            admin: false
        };

        //mock login
        switch (credentials.email.value) {
            case 'agent@consulaire.fr':
                retour.user.nom = "John"
                retour.user.prenom = "Doe"
                retour.user.email = "agent@consulaire.fr"
                break;
            case 'admin@consulaire.fr':
                retour.user.nom = "Jean"
                retour.user.prenom = "Admin"
                retour.user.email = "admin@consulaire.fr"
                retour.admin = true
                break;
            case 'superadmin@consulaire.fr':
                retour.user.nom = "Paul"
                retour.user.prenom = "Superadmin"
                retour.user.email = "superaadmin@consulaire.fr"
                retour.superadmin = true
                break;
            default:
                retour.user = null
        }
        
        return retour;

    }

}