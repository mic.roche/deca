import axios from 'axios'

const API_URL = process.env.API_URL || ''
const apiClient = axios.create({
    baseURL: API_URL,
    withCredentials: false,
})


export default {
    async fetchUsers() {
        const response = await apiClient.get("usersList.json");
        return response.data
    },
    // async getUsers(nom, departement, reseau) {
    //     console.log("Utilisateurs pour :" + nom + " " + departement + " " + reseau);
    //     const response = await apiClient.get("contratsSuite.json");
    //     return response.data;

    // },
    async AddUser(nom, prenom, mail, reseau, departement, admin = false) {
        console.log("Utilisateur ajouté :" + nom + " " + prenom + " " + mail + " " + reseau + " " + departement + " " + admin);


        return true;

    }
}