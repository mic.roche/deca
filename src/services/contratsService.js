import axios from 'axios'

const API_URL = process.env.API_URL || ''
const apiClient = axios.create({
    baseURL: API_URL,
    withCredentials: false,
})


export default {
    async getContrats(prenom, nom, dateNaissance, departement) {
        if (nom=="roche")
        return null

        console.log("Recherche de contrats pour :" + nom + " " + prenom + " " + dateNaissance + " " + departement);
        const response = await apiClient.get("contratsList.json");
        return response.data;

    },
    async getHistory(numContrat, siret) {
        console.log("Historique pour :" + numContrat + " " + siret);
        const response = await apiClient.get("contratsSuite.json");
        return response.data;

    },
    async getContrat(numContrat, siret, avenant = 0) {
        console.log("Detail pour :" + numContrat + " " + siret + " " + avenant);
        const response = await apiClient.get("contrat.json");
       
        return response.data;

    }
}