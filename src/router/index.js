import {
  createRouter,
  createWebHashHistory
} from 'vue-router';
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Logout from '../views/Logout.vue'
import Admin from '../views/Admin.vue'
import Error from '../views/Error.vue'
import Contrats from '../views/Contrats.vue'
import Contrat from '../views/Contrat.vue'
import store from '@/store';

const routes = [{
    path: "/",
    name: "Login",
    component: Login
  },
  {
    path: "/logout",
    name: "Logout",
    component: Logout
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/contrats",
    name: "contrats",
    component: Contrats,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/contrat/:numContrat/:siret/:avenant",
    name: "contrat",
    component: Contrat,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/admin",
    name: "Admin",
    component: Admin,
    meta: {
      requiresAdmin: true
    }
  },
  {
    path: "/error",
    name: "Error",
    component: Error,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})


router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.loggedIn) {
    next('/')

  }
  if (to.matched.some(record => record.meta.requiresAdmin) && !(store.getters.isAdmin || store.getters.isSuperAdmin)) {
    next('/error')
  }
  if (!(to.matched.some(record => record.meta.requiresAuth) && !store.getters.loggedIn) && !(to.matched.some(record => record.meta.requiresAdmin) && !(store.getters.isAdmin || store.getters.isSuperAdmin)) && !(to.matched.some(record => record.meta.requiresSuperAdmin) && !store.getters.isSuperAdmin)) {
    next()
  }
})

export default router