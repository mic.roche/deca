import Vuex from 'vuex'
import authService from '@/services/authService'
import usersService from '@/services/usersService'

export default Vuex.createStore({
  state: {
    user: null,
    superadmin: false,
    admin: false,
    authentificated: false,
    users: []

  },
  mutations: {
    SET_USER_DATA(state, userData) {
      state.user = userData
      state.authentificated = true
    },
    SET_ADMIN(state) {
      state.admin = true
    },
    SET_SUPERADMIN(state){
      state.superadmin =true
    },
    CLEAR_USER_DATA(state){
      state.user = null
      state.superadmin = false
      state.admin = false
      state.authenticated = false
    },
    SET_USERS(state,users){
      state.users = users
    },
    ADD_USER(state,user){
      state.users.push(user)
    },
    REMOVE_USER(state,user){
    const index = state.users.indexOf(user);
    if (index > -1) {
      state.users.splice(index, 1);
    }
    },
  },
  actions: {
    async login({
      commit
    }, credentials) {
      const data = await authService.login(credentials)
      commit('SET_USER_DATA', data.user)
      if(data.admin)
      commit('SET_ADMIN')
      if(data.superadmin)
      commit('SET_SUPERADMIN')      
     
      return true
    },
    async setUsers({commit}) {
      const users = await usersService.fetchUsers()
      commit('SET_USERS', users)
      return true
    },
    async addUser({
      commit
    }, userdata) {
      commit('ADD_USER', userdata)
      return true
    },
    async removeUser({
      commit
    }, userdata) {
      commit('REMOVE_USER', userdata)
      return true
    },
    logout({
      commit
    }) {
      commit('CLEAR_USER_DATA')
    }

  },
  getters: {
    loggedIn(state) {
      return state.authentificated
    },
    getUser(state) {
      return state.user
    },
    isAdmin(state) {
      return state.admin
    },
    isSuperAdmin(state) {
      return state.superadmin
    },
    getUsers(state){
      return state.users
    }

  }

});
